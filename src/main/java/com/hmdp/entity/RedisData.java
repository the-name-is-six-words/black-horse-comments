package com.hmdp.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author yu yang
 * @date 2022/10/11 -21:32
 */
@Data
public class RedisData {
    private LocalDateTime expireTime;
    private Object data;
}
