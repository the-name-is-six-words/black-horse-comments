package com.hmdp.service;

/**
 * @author yu yang
 * @date 2022/10/13 -19:53
 */
public interface ILock {
    boolean tryLock(Long timeSec);
    void unLock();
}
