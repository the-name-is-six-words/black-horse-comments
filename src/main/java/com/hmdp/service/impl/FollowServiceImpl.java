package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Follow;
import com.hmdp.mapper.FollowMapper;
import com.hmdp.service.IFollowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.service.IUserService;
import com.hmdp.utils.UserHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class FollowServiceImpl extends ServiceImpl<FollowMapper, Follow> implements IFollowService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private IUserService userService;
    @Override
    public Result follow(Long followId, Boolean isFollow) {
        Long userId = UserHolder.getUser().getId();
        String key = "common:"+userId;
        if (isFollow){
            //关注，新增关注信息
            Follow follow = new Follow();
            follow.setUserId(userId);
            follow.setFollowUserId(followId);
            boolean isSuccess = this.save(follow);
            if (isSuccess) {
                // 把关注用户的id，放入redis的set集合 sadd userId followerUserId
                stringRedisTemplate.opsForSet().add(key, followId.toString());
            }
        }else {
            //取关，删除
            boolean isSuccess = this.remove(new LambdaUpdateWrapper<Follow>().eq(Follow::getFollowUserId, followId).eq(Follow::getUserId, userId));
            if (isSuccess){
                stringRedisTemplate.opsForSet().remove(key,followId.toString());
            }
        }
        return Result.ok();
    }

    @Override
    public Result isFollowed(Long followId) {
        //是否被关注
        Long userId = UserHolder.getUser().getId();
        return Result.ok(this.lambdaQuery().eq(Follow::getFollowUserId,followId).eq(Follow::getUserId,userId).count()>0);
    }

    @Override
    public Result getCommon(Long id) {
        Long userId = UserHolder.getUser().getId();
        String key = "common:"+userId;
        String commonKey = "common:"+id;
        Set<String> intersect = stringRedisTemplate.opsForSet().intersect(key, commonKey);
        if (intersect == null || intersect.isEmpty()){
            return Result.ok(Collections.emptyList());
        }
        List<Long> ids = intersect.stream().map(Long::valueOf).collect(Collectors.toList());
        List<UserDTO> userDTOS = userService.listByIds(ids).stream()
                .map(user -> BeanUtil.copyProperties(user, UserDTO.class))
                .collect(Collectors.toList());

        return Result.ok(userDTOS);
    }
}
