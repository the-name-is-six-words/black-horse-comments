package com.hmdp;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Shop;
import com.hmdp.entity.User;
import com.hmdp.service.IShopService;
import com.hmdp.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.hmdp.utils.RedisConstants.LOGIN_USER_KEY;
import static com.hmdp.utils.RedisConstants.LOGIN_USER_TTL;

/**
 * @author yu yang
 * @date 2022/10/15 -19:51
 */

@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisDataAddTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private IUserService iUserService;
    @Autowired
    private IShopService shopService;
    @Test
    public void test(){
        // 根据电话号码从数据库中查询用户信息
        List<User> users = iUserService.query().list();
        users.forEach(user -> {
            // 隐藏用户信息
            UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
            //生成令牌作为存储在redis中用户信息的key
            String token = UUID.randomUUID().toString();
            System.out.println(token);
            Map<String, Object> userMap = BeanUtil.beanToMap(userDTO, new HashMap<>(),
                    CopyOptions.create()
                            .setIgnoreNullValue(true)
                            .setFieldValueEditor((fieldName, fieldValue) -> fieldValue.toString()));
            // 将用户信息存入redis中
            String tokenKey = LOGIN_USER_KEY + token;
            stringRedisTemplate.opsForHash().putAll(tokenKey,userMap);
            stringRedisTemplate.expire(tokenKey,LOGIN_USER_TTL, TimeUnit.MINUTES);
        });
    }

    @Test
    public void test1(){
        List<Shop> shopList = shopService.list();
        //将shop按照shop的类型分类
        Map<Long,List<Shop>> shopMap =shopList.stream().collect(Collectors.groupingBy(Shop::getTypeId));
        //将map添加到redis中
        String keyPrefix = "shop:geo:";
        for (Map.Entry<Long, List<Shop>> entry : shopMap.entrySet()) {
            //店铺类型
            Long shopTypeId = entry.getKey();
            List<Shop> shops = entry.getValue();

            List<RedisGeoCommands.GeoLocation<String>> geoLocations = new ArrayList<>(shops.size());
            //将每个shop转为GEOLocation类型

            shops.forEach(shop -> geoLocations.add(new RedisGeoCommands.GeoLocation<>(shop.getId().toString(),new Point(shop.getX(),shop.getY()))));
            stringRedisTemplate.opsForGeo().add(keyPrefix+shopTypeId,geoLocations);
        }
    }
    @Test
    public void test2(){
        String[] value = new String[1000];
        int j = 0;
        for (int i = 0; i < 1000000; i++) {
            j = i % 1000;
            value[j] = "user_" + i;
            if (j == 999){
                stringRedisTemplate.opsForHyperLogLog().add("hll1",value);
            }
        }
        Long hll1 = stringRedisTemplate.opsForHyperLogLog().size("hll1");
        System.out.println(hll1);
    }

}
